
Drupal.DruBB = {};
Drupal.DruBB.url = '';

Drupal.behaviors.drubb = function() {
  $('input.drubb-attach-image:not(.drubb-processed)').
    addClass('drubb-processed')
    .click(function() {
      // Figure out what the URL for this button is and stuff it into a private
      // variable so that the BUEditor button can find it. This is probably
      // cheating, but it should work. Maybe.
      Drupal.DruBB.url = $('small', $(this).prev()).html();
      // Maybe there should be some way to discover this, but we're going
      // to hardcode this for the moment.
      BUE.buttonClick(0, 0);
      return false;
    });
};